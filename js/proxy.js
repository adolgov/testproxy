var http = require('http'),
    httpProxy = require('http-proxy'),
    colors = require('colors'),
    connect = require('connect'),
    url = require('url'),
    harmon = require('harmon');

var selects = [];
var simpleselect = {};
var SCRIPT = '<script>!function(e){function n(e,n,o){e.addEventListener?e.addEventListener(n,o,!1):e.attachEvent&&e.attachEvent("on"+n,o)}n(e,"error",function(n){n=n||e.event,(new Image).src="http://localhost:9001?browser=%BROWSER%&location="+e.location.href+"&err="+n.message+"&file="+n.filename+"&line="+n.lineno+"&col="+n.colno})}(window);</script>';

simpleselect.query = 'head';
simpleselect.func = function (node, req, res) {  
  var browser = url.parse(req.url,true).query.browser,
      s = '',
      stream = node.createStream(),
      script = browser ? SCRIPT.replace("%BROWSER%", browser) : SCRIPT;

  stream.on('data', function(chunk) { s += chunk.toString(); });
  stream.on('end', function() {
    stream.end(script + s);
  });
} 

selects.push(simpleselect);

var app = connect();
var proxy = httpProxy.createProxyServer();
proxy.on('proxyRes', function (proxyRes, req, res) {

  // handle unrestricted requests
  if (url.parse(req.url).hostname != 'r.mradx.net'){
    console.log(req.url.red)
  }
});

proxy.on('error', function (err,req, res){
  console.log(err.red);
  res.end();
});

app.use(harmon([], selects, true));
app.use(function (req, res) {
  proxy.web(req, res, {target: req.url, prependPath: false});
})


http.createServer(app).listen(8000);


http.createServer(function (req, res) {
  var query = url.parse(req.url, true).query;
  console.log('\n================================================\n'.grey);
  console.log(query.browser.cyan);
  console.log(query.err.cyan);
  console.log(query.file.cyan + " " + query.line.blue + ":" + query.col.blue);
  res.end();
}).listen(9001);


// logging out what the fuck is goign on
process.stdout.write('\033c');
console.log('Staring...'.yellow);
console.log('http proxy server'.blue + ' started '.green + 'on port '.blue + '8000'.yellow);
console.log('http logging server'.magenta + ' started '.green + 'on port '.blue + '9001'.yellow);

// setTimeout(
//   function(){
//     console.log('Exiting...'.yellow);
//     process.exit(0)
// }, 5000);