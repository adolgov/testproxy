#!/usr/bin/env python
# -*- coding: utf-8 -*-

# TODO: 
# 1. Update report with each PRINT for async
# 2. Upload images instead of saving
# 3. Add CPU testing
# 4. Check if there is any possibility to test clicks with default webdriver (opera + safari)
# (they sayd that selenium works better on 10.10.2, Safari 8.0.3 Selenium 2.45)

from __future__ import print_function

import os
import sys
import json
import time
import urlparse
from threading import Thread
from termcolor import colored
from selenium import webdriver
from PIL import Image
from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
from SocketServer import ThreadingMixIn

from libmproxy import controller, proxy
from libmproxy.models import decoded
from libmproxy.proxy.server import ProxyServer

# TOOLS
print = lambda x: sys.stdout.write("%s\n" % x)
def cls(): os.system('cls' if os.name=='nt' else 'clear')

""" ================ CLASSES DEF ================= """

class TestProxy(controller.Master):

  script_string = '<script>!function(e){function n(e,n,o){e.addEventListener?e.addEventListener(n,o,!1):e.attachEvent&&e.attachEvent("on"+n,o)}n(e,"error",function(n){n=n||e.event,(new Image).src="http://127.0.0.1:9001/errors/?browser=%BROWSER%&location="+e.location.href+"&err="+n.message+"&file="+n.filename+"&line="+n.lineno+"&col="+n.colno})}(window);</script>'

  def __init__(self, server):
    controller.Master.__init__(self, server)    

  def run(self):
    try:
      return controller.Master.run(self)
    except KeyboardInterrupt:
      self.shutdown()

  def handle_request(self, flow):
    global requests_collected
    global unallowed_requests
    global clicks_delta

    # handle banned resources
    if (not flow.request.host in allowed_domains):
      if (not flow.request.url in unallowed_requests): 
        unallowed_requests.append (flow.request.url)
        print(colored('Unallowed request: ', 'yellow') + colored(flow.request.url, 'red'))

    # handle valid requests
    elif (flow.request.host == 'r.mradx.net'):
      if (not flow.request.url in requests_collected): 
        valid_url = flow.request.url
        if ('?' in valid_url):  valid_url = valid_url[:valid_url.find('?')]
        requests_collected.append (valid_url)
        print(colored('Valid request: ', 'blue') + colored(valid_url, 'magenta'))

    # handle clicks
    elif (flow.request.host == 'r.mail.ru'):
      if (flow.request.url == 'http://r.mail.ru/redir/EioI9PilCBCOl8lPHYgQ41UoBKABgNn56Aq4AUbCAgTTYPwDCJ7Hg2bAAQE'):
        clicks_delta = clicks_delta - 1
        # print(colored('Link1: ', 'cyan') + colored('checked!', 'green'))
    
    flow.reply()

  def handle_response(self, flow): 
    hid = (flow.request.host, flow.request.port)
    if ('content-type' in flow.response.headers and 'text/html' in flow.response.headers['content-type']):
      with decoded(flow.response):
        query = urlparse.parse_qs(urlparse.urlparse(flow.request.url).query)
        script_string = self.script_string
        if ('browser' in query):
          script_string = script_string.replace('%BROWSER%', str(query['browser'][0]))
        flow.response.content = flow.response.content.replace('<head>','<head>'+script_string)
    flow.reply()


# logging server for clicks and errors

class RequestHandler(BaseHTTPRequestHandler):  
  def do_GET(self):
    
    global js_errors

    path = urlparse.urlparse(self.path)
    
    # ignore favicon
    if (path.path == '/favicon.ico'):
      return
    
    # handle JS errors
    elif (path.path == '/errors/'):
      error = dict(urlparse.parse_qsl(path.query))
      error['location'] = error['location'][:error['location'].find('?')]
      js_errors.append(error)
      print (colored("JS Error: ", 'red') + str(error))

    elif (path.path == '/'):
      pass
    # some wiered requests that should not to be here, 
    # do I need to log it?  
    else:
      print (str(path))
    self.send_response(200)
    self.connection.close()

  def log_message(self, format, *args):
    return

""" ================ GLOBAL STUFF ================= """

def run_proxy(proxy_class):
  config = proxy_class.ProxyConfig(port=8000)
  server = ProxyServer(config)
  proxy = TestProxy(server)
  proxy.run()

def run_logger():
  serveraddr = ('localhost', 9001)
  logger = HTTPServer(serveraddr, RequestHandler)
  logger.serve_forever()

# "http://r.mradx.net/h5/66/98C666E7/935F6D7CD6.html"
# url = "http://r.mradx.net/h5/68/F89A8B7B/41D096414A.html" 
screenshots_path = "screenshots/"
link1_appendix = "&link1=213967774&data=CPT4pQgQjpfJTx2IEONVKASgAYDZ-egKuAFGwgIE02D8Aw"
url = "http://r.mradx.net/h5/51/4326201A/D0EBF0F3FC.html"
allowed_domains = ["127.0.0.1", "r.mail.ru", "r.mradx.net"]
clicks_delta = 0
meta = ""
requests_collected = []
unallowed_requests = []
js_errors = []
offset_x = 0
offset_y = 0
width = 240
height = 400

def crop_img(img_name):
  img = Image.open(img_name)
  img = img.crop((offset_x,offset_y,width,height))
  img.save(img_name)

# Create a new instance of the all browser drivers
browsers = {
  # 'ie': 'webdriver.Ie("C:\Selenium\IEDriverServer.exe")',
  # 'chrome': 'webdriver.Chrome("C:\Selenium\chromedriver.exe")',  
  # 'opera': 'webdriver.Opera(executable_path=r"C:\Selenium\operadriver.exe")',
  'chrome': 'webdriver.Chrome()',
  'safari': 'webdriver.Remote("http://127.0.0.1:4444/wd/hub", webdriver.DesiredCapabilities.SAFARI)',
  'firefox': 'webdriver.Firefox()'
}

# iterate all browsers to capture screenshots
def run_browsers_test():
  
  global clicks_delta
  global meta
  global offset_x
  global offset_y

  for browser_name in browsers:
    browser = eval(browsers[browser_name])
    browser.get(url + '?browser=' + browser_name + link1_appendix)
    time.sleep(1)
    
    if (not meta):
      meta = browser.execute_script('var meta; if(meta=document.querySelector(\'meta[name="ad.size"]\')) {return meta.getAttribute("content")} else {return ""}')      
      body = browser.find_element_by_tag_name('body')
      if (body):
        offset_x = body.location['x']
        offset_y = body.location['y']
      if (meta):
        meta = meta.lower().split(',')
        width = meta[0].split('=')[1]
        height = meta[1].split('=')[1]
      else:
        meta = 'failed'
        print(colored('no META info: considered default 240x400','red'));
    try:
      for i in range(0, 10):        
        browser.save_screenshot(screenshots_path + browser_name + '_' +str(i)+ '.png');
        crop_img(screenshots_path + browser_name + '_'+str(i)+'.png')
        # crutch for SAFARI driver that doesn't support clicks somehow and probably OPERA
        if (browser_name != 'safari'):
          body = browser.find_element_by_tag_name('body')
          click = webdriver.common.action_chains.ActionChains(browser)        
          click.move_to_element_with_offset(body, 50, 50)
          click.click()
          click.perform()
          clicks_delta = clicks_delta + 1
        time.sleep(1)
    finally:
      browser.quit()



""" ================ LAUNCHING EVERYTHING ================= """

# Running servers as daemons 

proxy_thread = Thread(target=run_proxy, args=(proxy,))
proxy_thread.daemon = True
proxy_thread.start()

logger_thread = Thread(target=run_logger)
logger_thread.daemon = True
logger_thread.start()


# Clearing console befere printing smth out
cls()

print (colored("Starting test...\n", 'yellow'))

if (len(sys.argv) == 1):
  print (colored("WARNING: no URL passed, used testing one: ", "red") + url)
else:
  url = str(sys.argv[1])


run_browsers_test()


# clicks_delta > 0 = not all clicks handled
# clicks_delta < 0 = to much requests per one click

# Report on each print
print (colored("\nClicks delta: ", 'blue') + str(clicks_delta))
print (colored("Unallowed requests: ", 'blue') + str(unallowed_requests))
print (colored("Valud requests: ", 'blue') + str(requests_collected))
print (colored("Javascript errors: ", 'blue') + str(js_errors))
print (colored("\nDone!\n\n\n", 'green'))
sys.exit()


while True:
  time.sleep(1)